#include <cstdint>
#include <cstddef>
#include <windows.h>
#include <cstdio>
#include <vector>
#include <map>

#include <string>
#include <utility>
#include <memory>
#include "mysql.h"

#include "../fonline2238.h"

void RegisterFosql(asIScriptEngine* ase);
typedef ScriptString Str;

//Poor man's unique ptr implementation

template <typename T>
class managed_ptr
{
public:
	T* instance;

	managed_ptr(const managed_ptr& other)
	{
		instance = other.instance;
		if (instance)
			instance->addRef();
	}

	operator bool()
	{
		return (bool)(instance);
	}

	managed_ptr()
	{
		instance = nullptr;
	}
	~managed_ptr()
	{
		reset();
	}
	void reset()
	{
		if (instance)
			instance->releaseRef();
		instance = nullptr;
	}
	T* get() const
	{
		return instance;
	}
	T* operator ->() const
	{
		return instance;
	}

	template <typename T, typename A>
	friend managed_ptr<T> make_managed(A a);
};

//VERY poor man's make_unique
template <typename T, typename A>
managed_ptr<T> make_managed(A a)
{
	managed_ptr<T> t;
	t.instance = new T(a);
	return t;
}

template <typename T, size_t cnt>
class s_array
{
public:
	T data[cnt];
	static const size_t size = cnt;
	T& operator[](int c)
	{
		return data[c];
	}
	T* begin()
	{
		return data;
	}
	T* end()
	{
		return begin() + cnt;
	}
};



//Prepared statement binds container
//What an unholy abomination
class MysqlBinds
{
public:
	//Supported maximum arguments/results 
	static const int bindCount = 64;
	//The small buffer size (maximum size for primitive types)
	static const int smallBufferSize = 8;

	//larger buffers (for strings and blobs)
	s_array<std::string, bindCount> longBuffers;

	//length buffers 
	s_array<unsigned long, bindCount> lengthBuffers;

	//is_null buffers
	s_array<bool, bindCount> nullBuffers;

	//the actual bind structures
	MYSQL_BIND array[bindCount];

	//small buffers
	char buffers[bindCount * smallBufferSize];

	//clears all buffers for a single bind column
	//undef behaviour if col < 0 or col >= bindCount
	void clearBuffer(int col)
	{
		for (int i = 0; i < smallBufferSize; i++)
			buffers[smallBufferSize * col + i] = 0;
		longBuffers[col].clear();
		nullBuffers[col] = false;
		lengthBuffers[col] = 0;
	}

	//clear all buffers to default values
	void clear()
	{
		memset((void*)array, 0, sizeof(array));
		memset((void*)buffers, 0, sizeof(buffers));
		for (auto it = longBuffers.begin(); it != longBuffers.end(); it++)
			it->clear();
		for (auto it = lengthBuffers.begin(); it != lengthBuffers.end(); it++)
			*it = 0;
		for (auto it = nullBuffers.begin(); it != nullBuffers.end(); it++)
			*it = false;
	}




};



//Holder for MySQL prepared statements
class MysqlStmt
{
public:
	
	bool has_results;
	int refcnt;
	void addRef() {
		refcnt += 1;
	}
	void releaseRef() {
		refcnt -= 1;
		if (refcnt <= 0) delete this;
	}

	MysqlBinds binds;
	MysqlBinds results;

	MYSQL_STMT* stmt;
	int affected_rows;
	int param_count;
	int column_count;

	MysqlStmt(MYSQL_STMT* r)
	{
		refcnt = 1;
		stmt = r;
		has_results = false;
		updateParams();
		results.clear();
		binds.clear();
	}

	std::string error;

	void updateParams()
	{
		affected_rows = 0;
		if (stmt)
		{
			param_count = mysql_stmt_param_count(stmt);
			column_count = mysql_stmt_field_count(stmt);
		}
	}

	void _error(const std::string& err)
	{
		Log("FOSQL MysqlStmt error: %s\n", err.c_str());
		error = err;
	}

	ScriptString* getError()
	{
		return &ScriptString::Create(error.c_str());
	}


	~MysqlStmt()
	{
		if (stmt)
			mysql_stmt_close(stmt);
	}


	char* _bind_init(unsigned int col, enum_field_types type)
	{
		char* buffer = (binds.buffers + MysqlBinds::smallBufferSize * col);
		binds.array[col].buffer_type = type;
		binds.array[col].buffer = buffer;
		binds.array[col].buffer_length = 0;
		binds.array[col].is_null = 0;
		binds.array[col].length = 0;
		return buffer;
	}

	void _bind_init_long(unsigned int col, enum_field_types type, const char* data, size_t cnt)
	{
		binds.longBuffers[col].assign(data, cnt);

		char* buffer = (char*)binds.longBuffers[col].c_str();
		binds.array[col].buffer_type = type;
		binds.array[col].buffer = buffer;
		binds.array[col].buffer_length = cnt;
		binds.array[col].is_null = 0;
		binds.array[col].length = 0;
	}

	#define CheckBind if (col >= MysqlBinds::bindCount) { _error("Maximum bind count exceeded"); return false; }

	template <typename T>
	bool bindGeneric(uint col, T v, enum_field_types type)
	{
		CheckBind;

		char* buffer = _bind_init(col, MYSQL_TYPE_LONG);
		*(T*)buffer = v;
		return true;
	}

	bool bindInt(uint col, int i)
	{
		return bindGeneric(col, i, MYSQL_TYPE_LONG);
	}

	bool bindInt64(uint col, int64 i)
	{
		return bindGeneric(col, i, MYSQL_TYPE_LONGLONG);
	}

	bool bindDouble(uint col, double i)
	{
		return bindGeneric(col, i, MYSQL_TYPE_DOUBLE);
	}

	bool bindFloat(uint col, float i)
	{
		return bindGeneric(col, i, MYSQL_TYPE_FLOAT);
	}

	bool bindNull(uint col)
	{
		return bindGeneric(col, 0, MYSQL_TYPE_NULL);
	}

	bool clearBinds()
	{
		binds.clear();
		return false;
	}

	bool bindString(uint col, const Str* s)
	{
		CheckBind;

		_bind_init_long(col, MYSQL_TYPE_STRING, s->c_str(), s->length());
		return true;
	}


#define CheckStatement if (!stmt) { _error("Statement not initialized."); return false; }

	bool execute()
	{
		affected_rows = 0;
		CheckStatement
		
		if (mysql_stmt_bind_param(stmt, binds.array))
		{
			_error(mysql_stmt_error(stmt));
			return false;
		}
		if (mysql_stmt_execute(stmt))
		{
			_error(mysql_stmt_error(stmt));
			return false;
		}
		affected_rows = mysql_stmt_affected_rows(stmt);
		if (has_results)
			return bindResults();
		return true;
	}


	bool bindResultType(unsigned int col, int type)
	{
		CheckBind;
		char* buffer = (results.buffers + MysqlBinds::smallBufferSize * col);

		results.clearBuffer(col);
		if (type == MYSQL_TYPE_STRING)
		{
			results.longBuffers[col].resize(518, 0);

			buffer = (char*)results.longBuffers[col].c_str();
			results.array[col].buffer_length = 512;
		}
		else
		{
			switch (type)
			{
			case MYSQL_TYPE_FLOAT:
			case MYSQL_TYPE_DOUBLE:
			case MYSQL_TYPE_LONG:
			case MYSQL_TYPE_LONGLONG:
				break;
			default:
				_error("Invalid statement result type enumeration\n");
				return false;
			}
			results.array[col].buffer_length = 0;
		}

		results.array[col].buffer_type = (enum_field_types)type;
		results.array[col].buffer = buffer;
		results.array[col].length = &(results.lengthBuffers[col]);
		results.array[col].is_null = (my_bool*)&(results.nullBuffers[col]);
		results.array[col].error = 0;
		has_results = true;
		return true;
	}


	bool bindResults()
	{
		CheckStatement;

		int rval = mysql_stmt_bind_result(stmt, results.array);

		if (rval)
		{
			_error(mysql_stmt_error(stmt));
			return false;
		}
		has_results = true;
		return true;
	}

	bool fetchRow()
	{
		CheckStatement
		if (mysql_stmt_fetch(stmt))
		{
			return false;
		}
		return true;
	}

	template <int e>
	bool _result_check_validity(unsigned int col)
	{
		if (col >= column_count)
		{
			_error("Result set field index out of bounds");
			return false;
		}
		if ((int)(results.array[col].buffer_type) != e)
		{
			_error("Bound result set field type differs from requested");
			return false;
		}
		if (results.nullBuffers[col])
			return false;
		return true;
	}

	template <typename T, int e>
	bool _result_get_trivial(unsigned int col, T* to)
	{
		if (!_result_check_validity<e>(col))
			return false;
		char* buffer = (results.buffers + MysqlBinds::smallBufferSize * col);
		*to = *(T*)buffer;
		return true;
	}

	bool getInt(unsigned int col, int* to)
	{
		return _result_get_trivial<int, (int)(MYSQL_TYPE_LONG)>(col, to);
	}

	bool getInt64(unsigned int col, int64_t* to)
	{
		return _result_get_trivial<int64_t, (int)(MYSQL_TYPE_LONGLONG)>(col, to);
	}

	bool getFloat(unsigned int col, float* to)
	{
		return _result_get_trivial<float, (int)(MYSQL_TYPE_FLOAT)>(col, to);
	}

	bool getDouble(unsigned int col, double* to)
	{
		return _result_get_trivial<double, (int)(MYSQL_TYPE_DOUBLE)>(col, to);
	}

	ScriptString* getString(unsigned int col)
	{
		if (!_result_check_validity<MYSQL_TYPE_STRING>(col))
			return nullptr;
		return &ScriptString::Create(results.longBuffers[col].c_str());
	}
};


struct
{
	std::vector<std::pair<MysqlStmt*, std::string>> uninitializedStatements;
} Global;




//The "power" of C metaprogramming
//construct function pointers for all mysql functions

#define LOAD_FUNCTION_OP(name, ret, args, callsign) static ret (STDCALL * name ## _) args;
#include "loadFunctions.h"
#undef LOAD_FUNCTION_OP

//construct wrapper functions to call those function pointers
#define LOAD_FUNCTION_OP(name, ret, args, callsign) ret STDCALL name args { return name ## _  callsign; }
#include "loadFunctions.h"
#undef LOAD_FUNCTION_OP


#define FOSQLLog Log

static bool connected = false;
static bool dllLoaded = false;
static MYSQL* mysql = nullptr;

//Macros for returning if uninitialized 

#define CheckInitialized if (!mysql) return -12;
#define CheckInitialized_null if (!mysql) return nullptr;
#define CheckInitialized_void if (!mysql) return;
#define CheckInitialized_false if (!mysql) return false;


//Initialize mysql driver
EXPORT int _fosql_init()
{
	if (!dllLoaded)
		return -1;

	if (mysql)
		return 1;

	mysql = mysql_init(NULL);
	return 0;
}


//Close mysql driver in a safe manner
int _fosql_quiet_close()
{
	if (mysql)
	{
		mysql_close(mysql);
		mysql = nullptr;
	}
	return 0;
}

	
EXPORT int fosql_connect()
{
	return -1;
}





//Internal function to load all the stuff
static int load_mysql()
{
	if (dllLoaded)
		return 1;

	HMODULE dl = LoadLibrary(L"libmysql.dll");
	if (!dl)
	{
		FOSQLLog("FOSql: Error loading libmysql.dll\n");
		return -1;
	}
	

	dllLoaded = true;

	//More "powerful" C metaprogramming:
	//construct GetProcAddress calls to fill those function pointers
	//No error checking ... 
	//...
	//TODO ?
#define LOAD_FUNCTION_OP(name, ret, args, callsign) name ## _  = ((ret (STDCALL *) args) GetProcAddress(dl, #name));
#include "loadFunctions.h"
#undef LOAD_FUNCTION_OP

	_fosql_init();

	
	return 0;
}

//The DLL entry thing
FONLINE_DLL_ENTRY(compiler)
{
	connected = false;
	dllLoaded = false;
	mysql = nullptr;
	
	load_mysql();
	RegisterFosql(ASEngine);
}



int __stdcall DllMain(void* module, unsigned long reason, void* reserved)
{
	switch (reason)
	{
	case 1: // Process attach
		break;
	case 2: // Thread attach
		break;
	case 3: // Thread detach
		break;
	case 0: // Process detach
		_fosql_quiet_close();
		break;
	}
	return 1;
}

#include <cassert>


void doException(const char* msg)
{
	asIScriptContext* ctx = ScriptGetActiveContext();
	if (ctx)
		ctx->SetException(msg);
	else
		FOSQLLog("FOSql Error: %s\n", msg);
}

bool fosqlConnect(const Str& host, const Str& user, const Str& pass, const Str& db, unsigned int port)
{
	CheckInitialized_false;
	if (mysql_real_connect(mysql, host.c_str(), user.c_str(), pass.c_str(), db.c_str(), port, nullptr, 0) == nullptr)
	{
		doException(mysql_error(mysql));
		return false;
	}
	connected = true;

	for (auto p = Global.uninitializedStatements.begin(); p != Global.uninitializedStatements.end(); p++)
	{
		if (p->first->stmt)
			continue;
		auto stmt = mysql_stmt_init(mysql);
		std::string& query = p->second;
		if (!stmt)
		{
			doException("Failed to allocate prepared statement");
			return false;
		}

		if (mysql_stmt_prepare(stmt, query.c_str(), query.length()))
		{
			doException(mysql_stmt_error(stmt));
			mysql_stmt_close(stmt);
			return false;
		}
		p->first->stmt = stmt;
		p->first->updateParams();
	}
	Global.uninitializedStatements.clear();
	return true;
}

bool fosqlCommit()
{
	CheckInitialized_false;
	if (mysql_commit(mysql))
	{
		return false;
	}
	return true;
}


bool fosqlRollback()
{
	CheckInitialized_false;
	if (mysql_rollback(mysql))
	{
		return false;
	}
	return true;
}

bool fosqlAutoCommit(bool v)
{
	CheckInitialized_false;
	if (mysql_autocommit(mysql, v))
	{
		return false;
	}
	return true;
}

MysqlStmt* fosqlNewStmt(const ScriptString& query)
{
	CheckInitialized_null;

	if (!connected)
	{
		auto ms = new MysqlStmt(nullptr);
		Global.uninitializedStatements.push_back(std::pair<MysqlStmt*, std::string>(ms, query.c_std_str()));
		return ms;
	}
	
	auto stmt = mysql_stmt_init(mysql);
	if (!stmt)
	{
		doException("Failed to allocate prepared statement");
		return nullptr;
	}

	if (mysql_stmt_prepare(stmt, query.c_str(), query.length()))
	{
		doException(mysql_stmt_error(stmt));
		mysql_stmt_close(stmt);		
		return nullptr;
	}
	auto ms = new MysqlStmt(nullptr);

	return ms;
}

MysqlStmt* fosqlNewStmt_Binds(void* buffer)
{
	ScriptString& query = **(ScriptString**)buffer;
	auto ms = fosqlNewStmt(query);
	if (!ms)
		return nullptr;
	


	int* data = (int*)buffer;
	data += 2;
	uint size = ((int*)buffer)[1];
	for (uint i = 0; i < size; i++)
	{
		ms->bindResultType(i, data[i]);
	}
	return ms;
}


//Register stuff
void RegisterFosql(asIScriptEngine* ase)
{	
	int r = 0;
	r = ase->SetDefaultNamespace("SQL");
	assert(r >= 0);

	r = ase->RegisterObjectType("Statement", 0, asOBJ_REF);
	assert(r >= 0);

	//Reference counting functions
	r = ase->RegisterObjectBehaviour("Statement", asBEHAVE_ADDREF, "void f()", asMETHOD(MysqlStmt, addRef), asCALL_THISCALL);
	assert(r >= 0);

	r = ase->RegisterObjectBehaviour("Statement", asBEHAVE_RELEASE, "void f()", asMETHOD(MysqlStmt, releaseRef), asCALL_THISCALL);
	assert(r >= 0);

	//Garbage collector functions

	r = ase->RegisterObjectBehaviour("Statement", asBEHAVE_FACTORY, "Statement@ f(::string&)", asFUNCTION(fosqlNewStmt), asCALL_CDECL);
	assert(r >= 0);

	//Not in AS 2.26.0
	//r = ase->RegisterObjectBehaviour("Statement", asBEHAVE_LIST_FACTORY, "Statement@ f(int &in) {string, repeat int}", asFUNCTION(fosqlNewStmt_Binds), asCALL_CDECL);
	//assert(r >= 0);


	//Register methods

	r = ase->RegisterObjectMethod("Statement", "bool SetInt(uint, int)", asMETHOD(MysqlStmt, bindInt), asCALL_THISCALL);	assert(r >= 0);
	r = ase->RegisterObjectMethod("Statement", "bool SetInt64(uint, int64)", asMETHOD(MysqlStmt, bindInt64), asCALL_THISCALL);	assert(r >= 0);
	r = ase->RegisterObjectMethod("Statement", "bool SetDouble(uint, double)", asMETHOD(MysqlStmt, bindDouble), asCALL_THISCALL);	assert(r >= 0);
	r = ase->RegisterObjectMethod("Statement", "bool SetFloat(uint, float)", asMETHOD(MysqlStmt, bindFloat), asCALL_THISCALL);	assert(r >= 0);
	r = ase->RegisterObjectMethod("Statement", "bool SetString(uint, ::string&)", asMETHOD(MysqlStmt, bindString), asCALL_THISCALL);	assert(r >= 0);

	r = ase->RegisterObjectMethod("Statement", "bool ClearParams()", asMETHOD(MysqlStmt, clearBinds), asCALL_THISCALL);	assert(r >= 0);
	r = ase->RegisterObjectMethod("Statement", "::string@ GetError()", asMETHOD(MysqlStmt, getError), asCALL_THISCALL);	assert(r >= 0);

	r = ase->RegisterObjectMethod("Statement", "bool FetchRow()", asMETHOD(MysqlStmt, fetchRow), asCALL_THISCALL);	assert(r >= 0);
	r = ase->RegisterObjectMethod("Statement", "bool BindResultType(uint, int)", asMETHOD(MysqlStmt, bindResultType), asCALL_THISCALL);	assert(r >= 0);
	r = ase->RegisterObjectMethod("Statement", "bool BindResults()", asMETHOD(MysqlStmt, bindResults), asCALL_THISCALL);	assert(r >= 0);

	r = ase->RegisterObjectMethod("Statement", "bool GetInt(uint, int &out)", asMETHOD(MysqlStmt, getInt), asCALL_THISCALL);	assert(r >= 0);
	r = ase->RegisterObjectMethod("Statement", "bool GetInt64(uint, int64 &out)", asMETHOD(MysqlStmt, getInt64), asCALL_THISCALL);	assert(r >= 0);
	r = ase->RegisterObjectMethod("Statement", "bool GetDouble(uint, double &out)", asMETHOD(MysqlStmt, getDouble), asCALL_THISCALL);	assert(r >= 0);
	r = ase->RegisterObjectMethod("Statement", "bool GetFloat(uint, float &out)", asMETHOD(MysqlStmt, getFloat), asCALL_THISCALL);	assert(r >= 0);
	r = ase->RegisterObjectMethod("Statement", "::string@ GetString(uint)", asMETHOD(MysqlStmt, getString), asCALL_THISCALL);	assert(r >= 0);

	r = ase->RegisterObjectMethod("Statement", "bool Execute()", asMETHOD(MysqlStmt, execute), asCALL_THISCALL);	assert(r >= 0);
	
	//And properties
	r = ase->RegisterObjectProperty("Statement", "const int AffectedRows", asOFFSET(MysqlStmt, affected_rows));	assert(r >= 0);
	r = ase->RegisterObjectProperty("Statement", "const int ParamCount", asOFFSET(MysqlStmt, param_count));	assert(r >= 0);
	r = ase->RegisterObjectProperty("Statement", "const int ColumnCount", asOFFSET(MysqlStmt, column_count));	assert(r >= 0);

	r = ase->RegisterGlobalFunction(
		"bool Connect(const ::string &in host, const ::string &in user, const ::string &in pass, const ::string &in database, uint port = 3306)",
		asFUNCTION(fosqlConnect),
		asCALL_CDECL);

	assert(r >= 0);

	r = ase->RegisterGlobalFunction(
		"bool Commit()",
		asFUNCTION(fosqlCommit),
		asCALL_CDECL);

	assert(r >= 0);

	r = ase->RegisterGlobalFunction(
		"bool Rollback()",
		asFUNCTION(fosqlRollback),
		asCALL_CDECL);

	assert(r >= 0);

	r = ase->RegisterGlobalFunction(
		"bool SetAutoCommit(bool)",
		asFUNCTION(fosqlAutoCommit),
		asCALL_CDECL);

	assert(r >= 0);

	//Reset default namespace
	r = ase->SetDefaultNamespace("");
	assert(r >= 0);



	(void)(r);
}

