
#include "common.h"
#include "coroutine.h"
#include "../3rdParty/angelscript/source/as_scriptfunction.h"
#include "../3rdParty/angelscript/source/as_callfunc.h"

#include <cassert>

//Compilation flags to change behaviour:

/*
	ENABLE_LOCAL_VAR_SERIALIZATION

	Serializes Critter, Location, Map, Item and other server resources using
	their IDs. This avoids issues when a resource is freed during coroutine sleep
	period. Any local variables of previously mentioned types might become invalid
	due to that. This flag makes sure that these local variables are replaced with
	null when they become invalid, instead of staiying in invalid state.
*/

//#define ENABLE_LOCAL_VAR_SERIALIZATION

/*
	Alternative to the above: doesn't serialize the references when yielding the
	coroutine, but checks simply if their IsNotValid flag is set afterwards.
*/

#define ENABLE_ISNOTVALID_CHECK



/*
	ENABLE_LOCAL_VAR_REFERENCE_COUNTING

	If this flag is enabled, server resources are reference counted (as they
	should be). However this is somewhat experimental. Only has effect when
	ENABLE_LOCAL_VAR_SERIALIZATION or ENABLE_ISNOTVALID_CHECK is defined. 
*/

#define ENABLE_LOCAL_VAR_REFERENCE_COUNTING



//AngelScript black magic - will crash if done improperly
void  *(__cdecl *GetSystemIdGetterFunction(asIScriptEngine* ase, const char* decl))(uint)
{
	auto fun = static_cast<asCScriptFunction*>(ase->GetGlobalFunctionByDecl(decl));
	//Log("FUNCTION %d (%d) -> %d\n", fun->sysFuncIntf->callConv, fun->GetParamCount(), fun->GetReturnTypeId() & asTYPEID_MASK_SEQNBR);

	return (void*(*)(uint))(fun->sysFuncIntf->func);
}

void *GetFunctionPtr(asIScriptFunction* ifun)
{
	auto fun = static_cast<asCScriptFunction*>(ifun);

	return (fun->sysFuncIntf->func);
}

class ThisCallWrapper {
public:
	void __thiscall dostuff(){}
};

typedef void (ThisCallWrapper::*ThisCallFunction)();

void DoThisCall(void* instance, void* function)
{
	ThisCallFunction tcf;
	void** buf = (void**)(&tcf);
	*buf = function;
	ThisCallWrapper* tcw = (ThisCallWrapper*)instance;

	(tcw->*tcf)();
}


struct SerializeInfo {
	asUINT tid;
	asIObjectType* objectType;
	bool reset;
	void* (__cdecl *getFunction)(uint);
	asUINT idOffset;
	asUINT isNotValidOffset;
	void* addRef;
	void* release;

	SerializeInfo(){}

	SerializeInfo(asIScriptEngine* ase, const char* name, const char* getter)
	{
		addRef = nullptr;
		release = nullptr;
		reset = true;
		tid = ase->GetTypeIdByDecl(name);
		objectType = ase->GetObjectTypeById(tid);
		if (!tid || !objectType)
			Log("Failed retrieving base type %s information", name);

		auto bcnt = objectType->GetBehaviourCount();
		for (auto i = 0; i < bcnt; i++)
		{
			asEBehaviours btype;
			auto beh = objectType->GetBehaviourByIndex(i, &btype);
			//Log("Behaviour %d found for %s - %d\n", btype, name);

#ifdef ENABLE_LOCAL_VAR_REFERENCE_COUNTING
			if (btype == asBEHAVE_ADDREF)
			{
				addRef = GetFunctionPtr(beh);
			}
			if (btype == asBEHAVE_RELEASE)
			{
				release = GetFunctionPtr(beh);
			}
#endif
		}
		
		if (getter)
		{
			getFunction = GetSystemIdGetterFunction(ase, getter);
		}

		auto cnt = objectType->GetPropertyCount();
		bool foundId = false;
		bool foundValid = false;
		for (auto i = 0; i < cnt; i++)
		{
			const char* pname = nullptr;
			int ptid;
			int poffset;

			auto prop = objectType->GetProperty(i, &pname, &ptid, nullptr, &poffset);
			if (ptid == asTYPEID_UINT32)
			{
				if (strcmp(pname, "Id") == 0)
				{
					idOffset = poffset;
					foundId = true;
					continue;
				}
			}
			if (ptid == asTYPEID_BOOL)
			{
				if (strcmp(pname, "IsNotValid") == 0)
				{
					isNotValidOffset = poffset;
					foundValid = true;
					continue;
				}
			}
		}

		if (!foundId)
		{
		//	Log("Failed finding property Id in %s\n", name);
			getFunction = nullptr;
			idOffset = 0;
		}

		if (!foundValid)
		{
		//	Log("Failed finding property IsNotValid in %s\n", name);
			isNotValidOffset = 0;

		}
		else
		{
#ifdef ENABLE_ISNOTVALID_CHECK
			reset = false;
#endif	
		}

#ifdef ENABLE_LOCAL_VAR_SERIALIZATION
		if (getFunction)
			reset = false;
#endif

	}
};


struct DelegateSerializeInfo {
	asUINT tid;
	asIObjectType* type;
	//Pair {offset, typeid}
	std::vector<std::pair<asUINT, asUINT>> serializables;
	DelegateSerializeInfo() {};
	DelegateSerializeInfo(asIObjectType* type);
};


static struct {
	
	std::map<asUINT, SerializeInfo> SerializeInfos;
	std::map<asUINT, DelegateSerializeInfo> DelegateSerializeInfos;

	asUINT CoroutineTID;
	asIObjectType* CoroutineType;

	asUINT DelegateTID;
	asIObjectType* DelegateType;
	asIScriptFunction* DelegateFunction;

	std::map<CoroutineVar, ASCoroutine*> Coroutines;
	
	CoroutineVar LastId;
	ASCoroutine* ActiveCoroutine;

} Global;

DelegateSerializeInfo::DelegateSerializeInfo(asIObjectType* type) : type(type)
{
	tid = type->GetTypeId();
	auto cnt = type->GetPropertyCount();
	for (auto i = 0; i < cnt; i++)
	{
		int ptid, poffset;

		auto p = type->GetProperty(i, nullptr, &ptid, nullptr, &poffset);
		
		auto it = Global.SerializeInfos.find(ptid & asTYPEID_MASK_SEQNBR);
		if (it == Global.SerializeInfos.end())
			continue;
		//Log("Found %d in %s\n", it->first, type->GetName());
		std::pair<asUINT, asUINT> pair;
		pair.first = poffset;
		pair.second = it->first;
		serializables.push_back(pair);
	}
}

//Helper function to construct new ASCoroutine
static ASCoroutine* GenerateCoroutine(asIScriptEngine* ase, asIScriptContext* ctx)
{
	Global.LastId += 1;

	//Copy the userdata, it might be something important (and it is)
	auto* newctx = ase->CreateContext();
	newctx->SetUserData(ctx->GetUserData());

	ASCoroutine* coroutine = new ASCoroutine(newctx, Global.LastId);
	ase->NotifyGarbageCollectorOfNewObject(coroutine, Global.CoroutineType);
	return coroutine;
}

//Creates and initializes the coroutine
//Doesn't actually start the coroutine,
ASCoroutine* StartCoroutine(asIScriptFunction *func)
{
	auto ctx = ScriptGetActiveContext();
	auto ase = ctx->GetEngine();
	if (func == nullptr)
	{
		ctx->SetException("Null passed to StartCoroutine");
		return nullptr;
	}

	ASCoroutine* coroutine = GenerateCoroutine(ase, ctx);

	coroutine->context->Prepare(func);
	func->Release();
	return coroutine;
}

//Creates and initializes the coroutine with a delegate
//Same thing as above, except with delegate objects
ASCoroutine* StartCoroutine_DG(asIScriptObject *func)
{
	auto ctx = ScriptGetActiveContext();
	auto ase = ctx->GetEngine();
	if (func == nullptr)
	{
		ctx->SetException("Null passed to StartCoroutine");
		return nullptr;
	}

	ASCoroutine* coroutine = GenerateCoroutine(ase, ctx);

	coroutine->context->Prepare(Global.DelegateFunction);
	coroutine->context->SetObject(func);
	coroutine->delegate = func;
	coroutine->delegateTID = func->GetTypeId() & asTYPEID_MASK_SEQNBR;

	auto it = Global.DelegateSerializeInfos.find(coroutine->delegateTID);
	if (it == Global.DelegateSerializeInfos.end())
	{
		Global.DelegateSerializeInfos[coroutine->delegateTID] = DelegateSerializeInfo(func->GetObjectType());
	}



	return coroutine;
}

//Yield currently executing coroutine
void YieldCoroutine(CoroutineVar val)
{
	auto ctx = ScriptGetActiveContext();
	if (Global.ActiveCoroutine == nullptr)
	{
		ctx->SetException("Failed yielding");
		return;
	}
	Global.ActiveCoroutine->yieldVal = val;
	ctx->Suspend();
	
}

//Get coroutine by ID
ASCoroutine* GetCoroutine(CoroutineVar val)
{
	auto it = Global.Coroutines.find(val);
	if (it == Global.Coroutines.end())
		return nullptr;
	auto cr = it->second;
	cr->addRef();
	return cr;
}

//Run coroutine by ID
bool RunCoroutine(CoroutineVar val, CoroutineVar argument, CoroutineVar* output)
{
	auto cr = GetCoroutine(val);
	/*
	for (auto& it = Global.Coroutines.begin(); it != Global.Coroutines.end(); it++)
	{
		auto v = *it;
		Log("%d -> %d\n", v.first, v.second->isFinished() ? 1 : 0);
	}
	*/
	if (!cr)
		return false;
	
	*output = cr->run(argument);
	if (cr->isFinished())
	{
		cr->release();
		cr->releaseRef();
		return false;
	}
	cr->release();
	return true;
}

//Helper function for getting engine types
static void AddSerializeInfo(asIScriptEngine* ase, const char* name, const char* getter)
{
	SerializeInfo si(ase, name, getter);
	Global.SerializeInfos[si.tid  & asTYPEID_MASK_SEQNBR] = si;
	//Log("SerializeInfo %d -> %s\n", si.tid & asTYPEID_MASK_SEQNBR, name);
}

//Register all new types and stuff
void RegisterCoroutine(asIScriptEngine* ase)
{
	//Initialzie global state
	new (&Global.Coroutines) std::map<CoroutineVar, ASCoroutine*>();
	Global.LastId = 0;
	Global.ActiveCoroutine = nullptr;
	
	//Assert function that does first, then asserts

	#define do_assert(x) {auto res = x; assert(x);}

	//Store some useful type ids for later use
	AddSerializeInfo(ase, "Critter", "Critter@ GetCritter(uint)");
	AddSerializeInfo(ase, "Location", "Location@ GetLocation(uint)");
	AddSerializeInfo(ase, "Map", "Map@ GetMap(uint)");
	AddSerializeInfo(ase, "Item",  nullptr);
	AddSerializeInfo(ase, "NpcPlane", nullptr);
	AddSerializeInfo(ase, "Scenery", nullptr);

	int r = 0;

	//Set a default namespace for all the stuff below

	r = ase->SetDefaultNamespace("Coroutines");
	assert(r >= 0);

	//Register a function reference definition for coroutine functions
    r = ase->RegisterFuncdef(CoroutineVarStr " Function(" CoroutineVarStr ")");
    assert(r >= 0);

	//Register a new AngelScript type: a reference type that is garbage collected
	//The Coroutine must be garbage collected because it may hold cyclical references to itself
    r = ase->RegisterObjectType("Coroutine", 0, asOBJ_REF | asOBJ_GC);
    assert(r >= 0);

	//Reference counting functions
    r = ase->RegisterObjectBehaviour("Coroutine", asBEHAVE_ADDREF, "void f()", asMETHOD(ASCoroutine, addRef), asCALL_THISCALL);
    assert(r >= 0);

    r = ase->RegisterObjectBehaviour("Coroutine", asBEHAVE_RELEASE, "void f()", asMETHOD(ASCoroutine, release), asCALL_THISCALL);
    assert(r >= 0);

	//Garbage collector functions
	
    r = ase->RegisterObjectBehaviour("Coroutine", asBEHAVE_SETGCFLAG, "void f()", asMETHOD(ASCoroutine, setGCFlag), asCALL_THISCALL);
    assert(r >= 0);
    r = ase->RegisterObjectBehaviour("Coroutine", asBEHAVE_GETGCFLAG, "bool f()", asMETHOD(ASCoroutine, getGCFlag), asCALL_THISCALL);
    assert(r >= 0);
    r = ase->RegisterObjectBehaviour("Coroutine", asBEHAVE_GETREFCOUNT, "int f()", asMETHOD(ASCoroutine, getRefCount), asCALL_THISCALL);
    assert(r >= 0);
    r = ase->RegisterObjectBehaviour("Coroutine", asBEHAVE_ENUMREFS, "void f(int&in)", asMETHOD(ASCoroutine, enumReferences), asCALL_THISCALL);
    assert(r >= 0);
    r = ase->RegisterObjectBehaviour("Coroutine", asBEHAVE_RELEASEREFS, "void f(int&in)", asMETHOD(ASCoroutine, releaseAllReferences), asCALL_THISCALL);
    assert(r >= 0);


	//Register methods

    r = ase->RegisterObjectMethod("Coroutine", CoroutineVarStr " run(" CoroutineVarStr ")", asMETHOD(ASCoroutine, run), asCALL_THISCALL);
    assert(r >= 0);

    r = ase->RegisterObjectMethod("Coroutine", "bool isFinished()", asMETHOD(ASCoroutine, isFinished), asCALL_THISCALL);
    assert(r >= 0);

	r = ase->RegisterObjectMethod("Coroutine", "void holdRef()", asMETHOD(ASCoroutine, holdRef), asCALL_THISCALL);
	assert(r >= 0);

	r = ase->RegisterObjectMethod("Coroutine", "void releaseRef()", asMETHOD(ASCoroutine, releaseRef), asCALL_THISCALL);
	assert(r >= 0);

	//And properties
	r = ase->RegisterObjectProperty("Coroutine", "const " CoroutineVarStr " id", asOFFSET(ASCoroutine, id));
	assert(r >= 0);

	r = ase->RegisterObjectProperty("Coroutine", "const " CoroutineVarStr " data", asOFFSET(ASCoroutine, yieldVal));
	assert(r >= 0);


	do_assert(Global.CoroutineTID = ase->GetTypeIdByDecl("Coroutine"));
	do_assert(Global.CoroutineType = ase->GetObjectTypeById(Global.CoroutineTID));


	//Register Delegate interface
	ase->RegisterInterface("Delegate");
	ase->RegisterInterfaceMethod("Delegate", CoroutineVarStr " run(" CoroutineVarStr ")");

	do_assert(Global.DelegateTID = ase->GetTypeIdByDecl("Delegate"));
	do_assert(Global.DelegateType = ase->GetObjectTypeById(Global.DelegateTID));

	do_assert(Global.DelegateFunction = Global.DelegateType->GetMethodByDecl(CoroutineVarStr " run(" CoroutineVarStr ")"));
    

	//Register some global functions

    r = ase->RegisterGlobalFunction(
        "Coroutine@ Create(Function @func)",
		asFUNCTION(StartCoroutine),
        asCALL_CDECL);
    
    assert(r >= 0);


	r = ase->RegisterGlobalFunction(
		"Coroutine@ Create(Delegate @dg)",
		asFUNCTION(StartCoroutine_DG),
		asCALL_CDECL);

	assert(r >= 0);


    r = ase->RegisterGlobalFunction(
        "void Yield(" CoroutineVarStr ")",
        asFUNCTION(YieldCoroutine),
		asCALL_CDECL);
    assert(r >= 0);

	r = ase->RegisterGlobalFunction(
		"Coroutine@ Get(" CoroutineVarStr ")",
		asFUNCTION(GetCoroutine),
		asCALL_CDECL);
	assert(r >= 0);

	r = ase->RegisterGlobalFunction(
		"bool Run(" CoroutineVarStr ", " CoroutineVarStr ", " CoroutineVarStr " &out )",
		asFUNCTION(RunCoroutine),
		asCALL_CDECL);
	assert(r >= 0);


	//Reset default namespace
    r = ase->SetDefaultNamespace("");
    assert(r >= 0);


    
    (void)(r);
}

void ASCoroutine::addRef()
{
    ref += 1;
    gcFlag = false;
}

void ASCoroutine::release()
{
    gcFlag = false;

	ref -= 1;
    if (ref <= 0)
    {
        releaseAllReferences(nullptr);
        delete this;
    }
}

void ASCoroutine::setGCFlag()
{
    gcFlag = true;
}

bool ASCoroutine::getGCFlag()
{
    return gcFlag;
}

int ASCoroutine::getRefCount()
{
    return ref;
}

void ASCoroutine::holdRef()
{
	if (holdRefFlag)
		return;
	holdRefFlag = true;
	addRef();
}

void ASCoroutine::releaseRef()
{
	if (!holdRefFlag)
		return;
	holdRefFlag = false;
	release();
}


void ASCoroutine::enumReferences(asIScriptEngine* ase)
{
    if (context != nullptr)
        ase->GCEnumCallback(context);
}

void ASCoroutine::releaseAllReferences(asIScriptEngine*)
{
    if (context)
    {
		/*
		for (int i = 0; i < 100; i++)
			Log("FINISHED!!!!!!!!");
		*/
		finished = true;
        context->Release();
        context = nullptr;
		Global.Coroutines.erase(id);
		if (delegate)
			delegate->Release();
		delegate = nullptr;
    }
}

ASCoroutine::ASCoroutine(asIScriptContext* ctx, CoroutineVar id) : context(ctx), id(id)
{
	Global.Coroutines[id] = this;

	finished = false;
	startUps = 0;
	gcFlag = false;
	ref = 1;
	yieldVal = 0;
	holdRefFlag = false;
	delegate = nullptr;
	delegateTID = 0;
}

bool ASCoroutine::isFinished()
{
    return (finished || context == nullptr);
}

void ASCoroutine::walkStack(bool save)
{

	//The most dangerous thing: state serialization
	//Might break everything

	size_t idx = 0;
	auto release = [&](void* ptr, SerializeInfo& info) {
		if (info.release && *(void**)(ptr))
			DoThisCall(*(void**)(ptr), info.release);
	};
	auto addRef = [&](void* ptr, SerializeInfo& info) {
		if (info.addRef && *(void**)(ptr))
			DoThisCall(*(void**)(ptr), info.addRef);
	};


	auto saveId = [&](void* ptr, SerializeInfo& info) {
		if (info.reset)
		{
			release(ptr, info);
			*(void**)(ptr) = nullptr;
			return;
		}
#ifdef ENABLE_LOCAL_VAR_SERIALIZATION

			if (*(void**)(ptr) == nullptr)
			{
				serializedIds.push_back(0);
			}
			else
			{
				char* dptr = *(char**)ptr;
				dptr += info.idOffset;
				uint id = *(uint*)dptr;
				serializedIds.push_back(id);
				release(ptr, info);
				*(void**)(ptr) = nullptr;
			//	Log("SerIds += %u\n", id);

			}
#endif
	};

	auto loadId = [&](void* ptr, SerializeInfo& info) {
		if (info.reset)
			return;
#ifdef ENABLE_LOCAL_VAR_SERIALIZATION
		if (idx >= serializedIds.size())
		{
			Log("Internal coroutine error - state serialization size mismatch in coroutine.cpp\n");
			return;
		}
		if (*(void**)(ptr) != nullptr)
		{
			Log("Internal coroutine error - server resource not serialized in coroutine.cpp\n");
			return;
		}
		auto id = serializedIds[idx];
		idx += 1;
		if (id == 0)
		{
			return;
		}
		else
		{
			*(void**)(ptr) = info.getFunction(id);
			addRef(ptr, info);
		}
#else	
#ifdef ENABLE_ISNOTVALID_CHECK
		if (*(void**)(ptr) != nullptr)
		{
			char* dptr = *(char**)ptr;
			dptr += info.isNotValidOffset;
		//	Log("%d Isnotvalid???\n", *dptr);
			if (*dptr)
			{
		//		Log("Not valid...\n");
				release(ptr, info);
				*(void**)(ptr) = nullptr;
			}
		}
#endif
#endif
	};

	

	if (save)
	{
		serializedIds.clear();
	}
	else
	{
	}

	if (delegate)
	{
		auto dinfo = Global.DelegateSerializeInfos[delegateTID];
		for (uint i = 0; i < dinfo.serializables.size(); i++)
		{
			auto& p = dinfo.serializables[i];
			auto& info = Global.SerializeInfos[p.second];
			
			auto ptr = (void*)(((char*)delegate) + p.first);
			//Log("--- %d %d %d  == %d\n", p.first, delegate, ptr, *(void**)ptr);
			
			if ((info.tid & asTYPEID_MASK_SEQNBR) != p.second)
			{
				Log("TID Mismatch (%d != %d)in deserialize in coroutine.cpp\n", info.tid & asTYPEID_MASK_SEQNBR, p.second);
				continue;
			}
			
			if (save)
			{
				saveId(ptr, info);
			}
			else
			{
				loadId(ptr, info);
			}
			//Log("--->>> %d\n", *(void**)ptr);
		}
	}


	auto max = context->GetCallstackSize();
	for (auto i = 0; i < max; i++)
	{
		auto vc = context->GetVarCount(i);
		for (auto i2 = 0; i2 < vc; i2++)
		{
			auto tid = context->GetVarTypeId(i2, i) & asTYPEID_MASK_SEQNBR;
			auto ptr = context->GetAddressOfVar(i2, i);
			if (!ptr || tid == 0)
			{
				Log("Internal coroutine error Invalid ptr at stack walking in coroutine.cpp\n");
				continue;
			}
			auto it = Global.SerializeInfos.find(tid);
			if (it == Global.SerializeInfos.end())
				continue;


			if (save)
			{
				saveId(ptr, it->second);
			}
			else
			{
				loadId(ptr, it->second);
			}
		}
	}

	if (!save)
		serializedIds.clear();

}

void ASCoroutine::deserializeState() {
	walkStack(false);
}

void ASCoroutine::serializeState() {
	walkStack(true);
}

CoroutineVar ASCoroutine::run(CoroutineVar val)
{
    auto ctx = ScriptGetActiveContext();
	if (Global.ActiveCoroutine != nullptr)
	{
		ctx->SetException("Nested coroutine calls not supported");
		return 0;
	}
    if (finished)
    {
        ctx->SetException("Coroutine already finished");
        return 0;
    }
    if (context == nullptr)
    {
        ctx->SetException("Coroutine context already released");
        return 0;
    }
	CoroutineVar ret = 0;
	if (startUps == 0)
		context->SetArgDWord(0, val);
	else
		deserializeState();

	startUps += 1;

	Global.ActiveCoroutine = this;
	int r = context->Execute();
	Global.ActiveCoroutine = nullptr;

	serializeState();

	if (r == asEXECUTION_FINISHED)
	{
		r = context->GetReturnDWord();
		releaseAllReferences(nullptr);
		return ret;
	}

	if (r == asEXECUTION_EXCEPTION)
	{
		int ln, col;
		const char* sec;
		ln = context->GetExceptionLineNumber(&col, &sec);
		std::string message = std::string("Coroutine exception: ") + context->GetExceptionString();
		context->GetEngine()->WriteMessage(sec, ln, col, asMSGTYPE_WARNING, message.c_str());
		releaseAllReferences(nullptr);
		return 0;
	}

	if (r == asEXECUTION_SUSPENDED)
	{
		return yieldVal;
	}
	ctx->SetException("Internal coroutine error in coroutine.cpp\n");
	return 0;
}

