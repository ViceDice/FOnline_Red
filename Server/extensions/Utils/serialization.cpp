#include "common.h"
#include <json.h>
#include <cassert>

static int Global_StringTid = 0;
static int Global_Uint8ArrayTid = 0;
static bool Global_Initialized = false;

static void InitializeGlobals(asIScriptEngine* ase)
{
	if (Global_Initialized)
		return;
	
	Global_Initialized = true;
	Global_StringTid = ase->GetTypeIdByDecl("string");
	Global_Uint8ArrayTid = ase->GetTypeIdByDecl("uint8[]");
}

template<typename T>
bool fromRaw(void*ptr, Json::Value& json)
{
	json = Json::Value(*(T*)ptr);
	return true;
}

bool SerializeObjectTo(asIScriptContext* ctx, asIScriptEngine* ase, void* ptr, int tid, Json::Value& json);

bool SerializeArrayTo(asIScriptContext* ctx, asIScriptEngine* ase, ScriptArray* ptr, int tid, Json::Value& json)
{
	if (ptr == nullptr)
		return false;

	//The CScriptArray stores everything but primitives as references
	bool isReference = true;
	size_t size = sizeof(intptr_t);
	if (tid <= 11) //Angelscript typeids above 11 are not primitive types
	{
		size = ase->GetSizeOfPrimitiveType(tid);
		isReference = false;
	}

	json = Json::Value(Json::arrayValue);
	int a_size = ptr->GetSize();
	json.resize(a_size);
	if (a_size == 0)
		return true;
	char* data = (char*)(ptr->First());
	
	for (int i = 0; i < a_size; i++)
	{
		void* cptr = (void*)ptr->At(i);
		SerializeObjectTo(ctx, ase, cptr, tid, json[i]);
	}
	return true;
}



bool SerializeObjectTo(asIScriptContext* ctx, asIScriptEngine* ase, void* ptr, int tid, Json::Value& json)
{
	if (!ptr)
		return true;

	if (tid <= 11)
	{
		if (tid == asTYPEID_BOOL) return fromRaw<bool>(ptr, json);
		else if (tid == asTYPEID_INT8) return fromRaw<int8>(ptr, json);
		else if (tid == asTYPEID_UINT8) return fromRaw<uint8>(ptr, json);
		else if (tid == asTYPEID_INT16) return fromRaw<int16>(ptr, json);
		else if (tid == asTYPEID_UINT16) return fromRaw<uint16>(ptr, json);
		else if (tid == asTYPEID_INT32) return fromRaw<int>(ptr, json);
		else if (tid == asTYPEID_UINT32) return fromRaw<uint>(ptr, json);
		else if (tid == asTYPEID_INT64) return fromRaw<int64>(ptr, json);
		else if (tid == asTYPEID_UINT64) return fromRaw<uint64>(ptr, json);
		else if (tid == asTYPEID_FLOAT) return fromRaw<float>(ptr, json);
		else if (tid == asTYPEID_DOUBLE) return fromRaw<double>(ptr, json);
		else return false;
	}

	bool isHandle = (tid & asTYPEID_OBJHANDLE) != 0;

	if (isHandle)
	{
		ptr = *(void**)ptr;
		if (ptr == nullptr)
			return true;
	}

	if ((tid & asTYPEID_MASK_SEQNBR) == (Global_StringTid & asTYPEID_MASK_SEQNBR))
	{
		ScriptString* ptrs = (ScriptString*)ptr;	
		if (ptrs->length() == 0)
			json = Json::Value("");
		else
			json = Json::Value(ptrs->c_str());
		return true;
	}

	if ((tid & asTYPEID_SCRIPTOBJECT) != 0)
	{
		
		asIScriptObject* obj = (asIScriptObject*)ptr;

		json = Json::Value(Json::objectValue);

		for (size_t i = 0; i < obj->GetPropertyCount(); i++)
		{
			
			const char* name = obj->GetPropertyName(i);
			int typedid = obj->GetPropertyTypeId(i);;
			void* offset = obj->GetAddressOfProperty(i);
			
			SerializeObjectTo(ctx, ase, offset, typedid, json[name]);

		}
		
		
		return true;
	}

	if ((tid & asTYPEID_TEMPLATE) != 0)
	{
		asIObjectType* type = ase->GetObjectTypeById(tid);
		if (std::string(type->GetName()) == "array")
		{
			int subId = type->GetSubTypeId();
			return SerializeArrayTo(ctx, ase, (ScriptArray*)ptr, subId, json);
		}
		return false;

	}
	return false;
}

std::string SerializeToStdString(void* ptr, int typeId, bool pretty)
{
	auto ctx = ScriptGetActiveContext();
	auto ase = ctx->GetEngine();
	auto type = ase->GetObjectTypeById(typeId);

	InitializeGlobals(ase);

	Json::Value root;

	SerializeObjectTo(ctx, ase, ptr, typeId, root);

	std::string str;
	if (pretty)
	{
		Json::StyledWriter fw;
		str = fw.write(root);
	}
	else
	{
		Json::FastWriter fw;
		str = fw.write(root);
	}
	
	return str;
}


EXPORT ScriptString* Serialize(void* ptr, int typeId, bool pretty)
{
	std::string str = SerializeToStdString(ptr, typeId, pretty);
	ScriptString* out = &ScriptString::Create(str.c_str());
	return out;
}

EXPORT ScriptArray* SerializeToArray(void* ptr, int typeId, bool pretty)
{
	std::string str = SerializeToStdString(ptr, typeId, pretty);
	if (str.size() == 0)
		return nullptr;

	
	ScriptArray* out = (ScriptArray*)ASEngine->CreateScriptObject(Global_Uint8ArrayTid);
	out->Resize(str.size());
	char* begin = (char*)out->First();
	
	for (size_t i = 0; i < str.size(); i++)
		begin[i] = str[i];

	return out;
}


bool DeserializeTo(asIScriptContext* ctx, asIScriptEngine* ase, void* ptr, int tid, Json::Value& json);
bool DeserializeArrayTo(asIScriptContext* ctx, asIScriptEngine* ase, ScriptArray* ptr, int tid, Json::Value& json)
{
	if (json.type() != Json::arrayValue)
		return false;
	//The CScriptArray stores everything but primitives as references
	bool isReference = true;
	size_t size = sizeof(intptr_t);
	if (tid <= 11) //Angelscript typeids above 11 are not primitive types
	{
		size = ase->GetSizeOfPrimitiveType(tid);
		isReference = false;
	}
	
	int a_size = json.size();


	ptr->Resize(a_size);

	for (int i = 0; i < a_size; i++)
	{
		void* cptr = (void*)ptr->At(i);
		if (!DeserializeTo(ctx, ase, cptr, tid, json[i]))
			return false;
	}
	return true;
}


template<typename T>
bool deserializeRaw(void* ptr, int tid, Json::Value& json)
{
	if (sizeof(T) > 4)
		(*(T*)ptr) = json.asInt64();
	else
		(*(T*)ptr) = json.asInt();
	return true;
}


bool deserializeRawBool(void* ptr, int tid, Json::Value& json)
{
	(*(bool*)ptr) = json.asBool();
	return true;
}

template<typename T>
bool deserializeRawFloat(void* ptr, int tid, Json::Value& json)
{
	(*(T*)ptr) = json.asDouble();
	return true;
}


bool DeserializeTo(asIScriptContext* ctx, asIScriptEngine* ase, void* ptr, int tid, Json::Value& json)
{

	if (tid <= 11)
	{
		
		
		if (tid == asTYPEID_BOOL) return deserializeRawBool(ptr, tid, json);
		else if (tid == asTYPEID_INT8) return deserializeRaw<int8>(ptr, tid, json);
		else if (tid == asTYPEID_UINT8) return deserializeRaw<uint8>(ptr, tid, json);
		else if (tid == asTYPEID_INT16) return deserializeRaw<int16>(ptr, tid, json);
		else if (tid == asTYPEID_UINT16) return deserializeRaw<uint16>(ptr, tid, json);
		else if (tid == asTYPEID_INT32) return deserializeRaw<int>(ptr, tid, json);
		else if (tid == asTYPEID_UINT32) return deserializeRaw<uint>(ptr, tid, json);
		else if (tid == asTYPEID_INT64) return deserializeRaw<int64>(ptr, tid, json);
		else if (tid == asTYPEID_UINT64) return deserializeRaw<uint64>(ptr, tid, json);
		else if (tid == asTYPEID_FLOAT) return deserializeRawFloat<float>(ptr, tid, json);
		else if (tid == asTYPEID_DOUBLE) return deserializeRawFloat<double>(ptr, tid, json);
		else return false;
	}


	bool isHandle = (tid & asTYPEID_OBJHANDLE) != 0;
	void* hptr = ptr;
	if (isHandle)
	{
		hptr = *(void**)ptr;
		if (json.isNull())
		{
			if (hptr)
			{
				asIObjectType* type = ase->GetObjectTypeById(tid);
				ase->ReleaseScriptObject(hptr, type);
				*(void**)ptr = nullptr;
			}
			return true;
		}
	}
	else 
		if (json.isNull())
			return false;

	if ((tid & asTYPEID_MASK_SEQNBR) == (Global_StringTid & asTYPEID_MASK_SEQNBR))
	{
		if (isHandle)
		{
			if (!hptr)
			{
				*(ScriptString**)ptr = &ScriptString::Create(json.asCString());
				return true;
			}
		}
		ScriptString* ptrs = (ScriptString*)hptr;
		ptrs->assign(json.asCString());
		return true;
	}

	if (isHandle)
	{
		if (!hptr)
		{
			tid = tid & (~asTYPEID_OBJHANDLE);
			*(void**)ptr = ase->CreateScriptObject(tid);
			hptr = *(void**)ptr;
			if (!hptr)
			{
				return false;
			}
		}
	}

	if ((tid & asTYPEID_SCRIPTOBJECT) != 0)
	{
		asIScriptObject* obj = (asIScriptObject*)hptr;

		for (size_t i = 0; i < obj->GetPropertyCount(); i++)
		{
			const char* name = obj->GetPropertyName(i);
			int typedid = obj->GetPropertyTypeId(i);;
			void* offset = obj->GetAddressOfProperty(i);
			//Log("DSR %s %d %d\n", name, typedid, offset);
			if (DeserializeTo(ctx, ase, offset, typedid, json[name]))
			{
				//Log("Success!!!\n");
			}
			
		}

		return true;
	}

	if ((tid & asTYPEID_TEMPLATE) != 0)
	{
		
		asIObjectType* type = ase->GetObjectTypeById(tid);
		if (std::string(type->GetName()) == "array")
		{
			int subId = type->GetSubTypeId();
			return DeserializeArrayTo(ctx, ase, (ScriptArray*)hptr, subId, json);
		}
		return false;

	}
	return false;
}


bool DeserializeCstr(const char* start, const char* end, void* ptr, int typeId)
{
	auto ctx = ScriptGetActiveContext();
	auto ase = ctx->GetEngine();
	auto type = ase->GetObjectTypeById(typeId);
	InitializeGlobals(ase);

	if ((typeId & asTYPEID_OBJHANDLE) != 0)
	{
		ptr = *(void**)ptr;
		if (ptr == nullptr)
		{
			ctx->SetException("Deserialize called with null handle");
			return false;
		}
		typeId = typeId & (~asTYPEID_OBJHANDLE);
	}
	else
	{
		ctx->SetException("Deserialize should be called with an handle to an object");
		return false;
	}


	Json::Reader reader;
	Json::Value root;
	
	if (reader.parse(start, end, root))
	{
		return DeserializeTo(ctx, ase, ptr, typeId, root);
	}

	else
	{
		std::string s = std::string(start, (end - start));
		Log("Failed to parse JSON %s\n", s.c_str());
		return false;
	}

}

EXPORT bool Deserialize(ScriptString* from, void* ptr, int typeId)
{
	return DeserializeCstr(
		from->c_std_str().begin(),
		from->c_std_str().end(),
		ptr,
		typeId);
}

EXPORT bool DeserializeFromArray(ScriptArray* from, void* ptr, int typeId)
{
	if (from->GetSize() == 0)
		return false;
	char* begin = (char*)from->First();
	char* end = begin + from->GetSize();

	return DeserializeCstr(
		begin,
		end,
		ptr,
		typeId);
}


void RegisterSerialization(asIScriptEngine* ase)
{
	InitializeGlobals(ase);
	int r = 0;
	r = ase->SetDefaultNamespace("JSON");
	
	r = ase->RegisterGlobalFunction("void Serialize(?&in, string&)", asFUNCTION(Serialize), asCALL_CDECL);	

	r = ase->SetDefaultNamespace("");
	
}
