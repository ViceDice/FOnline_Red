#define CoroutineVar uint
#define CoroutineVarStr "uint"



extern void RegisterCoroutine(asIScriptEngine* ase);
class ASCoroutine
{
public:
    //! Increments reference counter
    void addRef();

    //! Decrements reference counter
    void release();


	//! "Safe" (single) reference counting stuff for AS
	void holdRef();
	void releaseRef();
	bool holdRefFlag;

    //! Returns true if the Coroutine has finished
    bool isFinished();


    //These functions are meant to be only called from AngelScript context
	CoroutineVar run(CoroutineVar);

    ASCoroutine(asIScriptContext* ctx, CoroutineVar id);

	asUINT delegateTID;
	asIScriptObject* delegate;
    asIScriptContext* context;
    bool finished;
    
    //GC stuff
    void setGCFlag();
    bool getGCFlag();
    int getRefCount();
    void enumReferences(asIScriptEngine*);
    void releaseAllReferences(asIScriptEngine*);
    
	int startUps;
    bool gcFlag;
    int ref;
	CoroutineVar id;
	CoroutineVar yieldVal;

	std::vector<uint> serializedIds;

	void deserializeState();
	void serializeState();
	void walkStack(bool save);
};