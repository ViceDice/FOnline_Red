#include "../fonline2238.h"

EXPORT void ShiftSprites(int x, int y) // useless
{
	uint count = 0;
	Sprite** sprites = FOnline->GetDrawingSprites(count);
	for(int i = 0; i<count; i++)
	{
		Sprite* currentSprite = sprites[i];

		(int&)(currentSprite->ScrX) += x;
		(int&)(currentSprite->ScrY) += y;
	}
}
