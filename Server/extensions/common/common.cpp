// This is the main DLL file.

#include "stdafx.h"

#include "common.h"



#ifdef __SERVER

#endif

#ifdef __CLIENT


EXPORT float GetAngle(uint16 hx, uint16 hy, uint16 tx, uint16 ty)
{
	return GetDirectionF(hx,  hy,  tx, ty);
}

#endif