@echo on
del C:\fubu\*.* /Q
xcopy "C:\fonlineforever\Server\save\clients\*.*" C:\fubu\ /c /s /r /d /y /i > C:\fubu\xcopy.log
xcopy "C:\fonlineforever\Server\save\clients\*.*" C:\fubu2\ /c /s /r /d /y /i > C:\fubu2\xcopy.log
set source=C:\fonlineforever\Server\save
set dest1=C:\fubu
set dest2=C:\fubu2
pushd "%source%"
for /f "tokens=*" %%G in ('dir *.fo /b /a-d /od') do SET "newest=%%G"

copy "%newest%" "%dest1%"
copy "%newest%" "%dest2%"
popd

